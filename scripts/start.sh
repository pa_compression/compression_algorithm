#!/bin/bash

export AWS_DEFAULT_REGION="eu-west-3"

pkill -f /opt/app/handler.py

nohup python3 -u /opt/app/handler.py > /tmp/output.log 2> /tmp/output.err &