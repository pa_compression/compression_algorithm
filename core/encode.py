# Files for encoding & decoding functions


from commons.structures import Node, Heap


# TODO UT
def generate_tree(text: str):
    """Huffman tree generator function

    Args:
        text (str): Text for which the tree is generated

    Returns:
        Node: Tree generated
    """
    heap = Heap()
    for c in text:
        encode.codes[ord(c)] += 1
    for counter, elem in enumerate(encode.codes):
        if elem != 0:
            heap.add(Node(chr(counter) if chr(counter) != '\n' else 'br', elem))
    while len(heap) != 1:
        min_left = heap.pop()
        min_right = heap.pop()
        merged = Node(None, min_left.weight + min_right.weight)
        merged.left_child = min_left
        merged.right_child = min_right
        heap.add(merged)
    return heap.pop()


def generate_tree_directory() -> Node:
    """Generate Huffman tree

    Returns:
        Node: Huffman tree
    """
    heap = Heap()
    for counter, elem in enumerate(encode.codes):
        if elem != 0:
            heap.add(Node(chr(counter) if chr(counter) != '\n' else 'br', elem))
    while len(heap) != 1:
        min_left = heap.pop()
        min_right = heap.pop()
        merged = Node(None, min_left.weight + min_right.weight)
        merged.left_child = min_left
        merged.right_child = min_right
        heap.add(merged)
    return heap.pop()


def build_tree(in_order: [str], pre_order: [str], in_start: int, in_end: int) -> Node:
    """Build tree from in order and pre order traversals

    Args:
        in_order ([str]): In order traversal
        pre_order ([str]): Pre order traversal

    Returns:
        Node: Huffman tree
    """
    if in_start > in_end:
        return None
    unpacked = pre_order[build_tree.pre_index].split(':')
    if len(unpacked) == 2:
        value, weight = unpacked
    else:
        value, weight = ':', unpacked[-1]
    if value == 'None':
        value = None
    elif value == 'br':
        value = '\n'
    res = Node(value, int(weight))
    build_tree.pre_index += 1
    if in_start == in_end:
        return res
    in_index = search(in_order, in_start, in_end, '{}:{}'.format(res.value if value != '\n' else 'br', res.weight))
    res.left_child = build_tree(in_order, pre_order, in_start, in_index - 1)
    res.right_child = build_tree(in_order, pre_order, in_index + 1, in_end)
    return res


def search(arr: [str], start: int, end: int, value: str) -> int:
    """Return index of value in arr"""
    for i in range(start, end + 1):
        if arr[i] == value:
            return i


def get_tree_from_file(pre_order_path: str, in_order_path: str) -> Node:
    """Build Huffman tree from in order and pre order traversals files

    Args:
        pre_order_path (str): Pre order display file path
        in_order_path (str): In order display file path

    Returns:
        Node: Huffman tree
    """
    with open(pre_order_path, encoding='utf-8') as f:
        pre_order = [elem.rstrip() for elem in f.readlines()]
    with open(in_order_path, encoding='utf-8') as f:
        in_order = [elem.rstrip() for elem in f.readlines()]
    build_tree.pre_index = 0
    return build_tree(in_order, pre_order, 0, len(pre_order) - 1)


# TODO UT
def update_codes(root: Node, code: str = ''):
    """Update char values with the given core

    Args:
        root (Node): Huffman core
        code (str): Current encoding
    """
    if not root:
        return
    if root.value:
        encode.codes[ord(root.value if root.value != 'br' else '\n')] = code
    else:
        update_codes(root.left_child, code=code + '0')
        update_codes(root.right_child, code=code + '1')


# TODO UT
def encode(text: str, huffman_tree: Node = None) -> (Node, str):
    """Encoding function

    Args:
        text (str): Text to encode
        huffman_tree (Node, Optional): Huffman tree

    Returns:
        (Node, str): (Huffman core, Encoded text)
    """
    if not huffman_tree:
        huffman_tree = generate_tree(text)
    update_codes(huffman_tree)
    res = ''
    try:
        for c in text:
            res += encode.codes[ord(c)]
    except IndexError:
        pass
    return huffman_tree, res


# TODO UT
def decode(code: str, tree: Node):
    """Huffman decoding function

    Args:
        code (str): Encoded text
        tree (Node): Huffman tree

    Returns:
        str: Decoded text
    """
    i = 0
    res = ''
    while i < len(code):
        p = tree
        while not p.value:
            p = p.left_child if code[i] == '0' else p.right_child
            i += 1
        res += p.value if p.value != 'br' else '\n'
    return res


if __name__ == '__main__':
    pass
