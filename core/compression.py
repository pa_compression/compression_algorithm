# File for compression & decompression functions


import os
from typing import Callable, Optional

from core.encode import encode, get_tree_from_file, decode, generate_tree_directory
from commons.enums import CompressionEnum
from commons.structures import Node


# TODO: UT
def encoded_to_bytes(encoded_string: str) -> (bytearray, int):
    """Convert the encoded string to bytearray

    Args:
        encoded_string: Encoded string by encode function

    Returns:
        bytearray: Encoded string in byte array
        int: Number of additional bytes
    """
    added_bytes = 0
    encoded_bytes = [encoded_string[i:i + 8] for i in range(0, len(encoded_string), 8)]
    if len(encoded_bytes[-1]) < 8:
        added_bytes = 8 - len(encoded_bytes[-1])
        encoded_bytes[-1] += '0' * added_bytes
    encoded_bytes = [int(bytes(elem, 'utf-8'), 2) for elem in encoded_bytes]
    res = bytearray()
    for elem in encoded_bytes:
        res.append(elem)
    return res, added_bytes


def update_char_count(path: str):
    """Update character count

    Args:
        path (str): Path to the directory to compress
    """
    if os.path.isfile(path):
        if os.stat(path).st_size == 0:
            return
        try:
            with open(path, encoding='utf-8') as f:
                text = f.read()
        except UnicodeDecodeError:
            try:
                with open(path) as f:
                    text = f.read()
            except UnicodeDecodeError:
                pass
        try:
            for c in text:
                try:
                    encode.codes[ord(c)] += 1
                except IndexError:
                    pass
        except UnboundLocalError:
            pass
    else:
        files = os.listdir(path)
        for file in files:
            update_char_count(os.path.join(path, file))


def create_tree(path: str) -> Node:
    """Create Huffman tree

    Args:
        path (str): Path to the directory

    Returns:
        Node: Huffman tree
    """
    encode.codes = [0] * 256
    update_char_count(path)
    return generate_tree_directory()


def directory_compression(path: str, tree: Node = None, path_prefix: str = None):
    """Compress a directory

    Args:
        path (str): Path to the directory
        tree (Node, Optional): Huffman tree
        path_prefix (str, Optional): Path where to put the directory
    """
    if not tree:
        tree = create_tree(path)
        name = os.path.split(os.path.splitext(path)[0])[1]
        with open(os.path.join(os.path.dirname(path), name + '_preorder'), 'w') as f:
            f.write(str(tree))
        with open(os.path.join(os.path.dirname(path), name + '_inorder'), 'w') as f:
            f.write(tree.in_order_display())
    if os.path.isfile(path):
        filename, added = compress_file(path, tree, path_prefix=path_prefix)
        root_path = os.path.dirname(path)
        if path_prefix:
            while root_path != path_prefix:
                root_path = os.path.dirname(root_path)
                path_prefix, name = os.path.split(path_prefix)
            root_path += '/' + '_'.join(name.split('_')[:-1])
        with open(root_path + '_added_bytes', 'a') as f:
            f.write('{} : {}\n'.format(path, added))
    else:
        new_dir = os.path.join(os.path.dirname(path) if not path_prefix else path_prefix,
                               os.path.split(path)[1] + '_compressed')
        os.mkdir(new_dir)
        files = os.listdir(path)
        for file in files:
            directory_compression(os.path.join(path, file), tree=tree, path_prefix=new_dir)


def compress_file(path: str, tree: Node, path_prefix: str = Node) -> (str, int):
    """Compress a file

    Args:
        path (str): Path to the file
        tree (Node): Huffman tree

    Returns:
        str: Path to the compressed file
        int: Number of added bytes
    """
    filename = os.path.splitext(path)[0]
    if not path_prefix:
        compressed_fn = filename + CompressionEnum.COMPRESSION.value
    else:
        new_path = os.path.join(path_prefix, os.path.split(filename)[1])
        compressed_fn = new_path + CompressionEnum.COMPRESSION.value
    if os.stat(path).st_size == 0:
        open(compressed_fn, 'w').close()
        return compressed_fn, 0
    with open(path) as f:
        content = f.read()
    tree, encoded_content = encode(content, huffman_tree=tree)
    encoded_content_to_bytes, added = encoded_to_bytes(encoded_content)
    with open(compressed_fn, 'wb') as f:
        f.write(encoded_content_to_bytes)
    return compressed_fn, added


def compress_single(path: str, tree: Node, dest: str, base_path: str):
    """Compress the file found at path to dest

    Args:
        path (str): Path to the file
        tree (Node): Huffman tree
        dest (str): Path to the destination file
        base_path (str): Path from the origin directory

    Raises:
        FileNotFoundError: Path not found
    """
    if not os.path.exists(path):
        raise FileNotFoundError('path not found')
    true_name = path[path.index(base_path) + len(base_path) + 1:]
    if os.stat(path).st_size == 0:
        with open(dest, 'ab') as f:
            f.write(bytearray('{}:None\n===\n'.format(true_name), 'utf-8'))
        return
    try:
        with open(path, encoding='utf-8') as f:
            content = f.read()
    except UnicodeDecodeError:
        try:
            with open(path) as f:
                content = f.read()
        except UnicodeDecodeError:
            pass
    try:
        tree, encoded_content = encode(content, huffman_tree=tree)
        encoded_content_to_bytes, added_bytes = encoded_to_bytes(encoded_content)
        with open(dest, 'ab') as f:
            f.write(bytearray('{}:{}\n'.format(true_name,
                                               added_bytes), 'utf-8'))
            f.write(encoded_content_to_bytes)
            f.write(bytearray('\n===\n', 'utf-8'))
    except UnboundLocalError:
        with open(dest, 'ab') as f:
            f.write(bytearray('{}:{}\n'.format(true_name,
                                               0), 'utf-8'))
            f.write(bytearray('\n===\n', 'utf-8'))


def dir_compression_to_file(path: str,
                            tree: Node = None,
                            dest: str = None,
                            base_path: str = None) -> str:
    """Compress a directory to dest

    Args:
        path (str): Path to the directory
        tree (Node, Optional): Huffman tree
        dest (str, Optional): Path to the destination file
        base_path (str, Optional): Path from the origin directory

    Returns:
        str: Path to the compressed file
    """
    name = os.path.split(os.path.splitext(path)[0])[1]
    dest_dir = os.path.join(os.path.dirname(path), name + '_compressed')
    if not dest:
        os.mkdir(dest_dir)
        dest = os.path.join(dest_dir, name + '_compressed')
    if not tree:
        tree = create_tree(path)
        with open(os.path.join(dest_dir, name + '_preorder'), 'w', encoding='utf-8') as f:
            f.write(str(tree))
        with open(os.path.join(dest_dir, name + '_inorder'), 'w', encoding='utf-8') as f:
            f.write(tree.in_order_display())
    if not base_path:
        base_path = os.path.split(path)[1]
    if os.path.isfile(path):
        compress_single(path, tree, dest, base_path)
    else:
        files = os.listdir(path)
        for file in files:
            dir_compression_to_file(os.path.join(path, file), tree, dest, base_path)
    return dest_dir


def decompress(encoded: bytearray, tree: Node, dest: str = None):
    """Decode and write to file

    Args:
        encoded (bytearray): Encoded filename and file content
        dest (str, Optional): Path where to put the file
    """
    if bytearray('\n', 'utf-8') in encoded:
        filename_end_index = encoded.index(bytearray('\n', 'utf-8'))
        filename, excess_bits = encoded[:filename_end_index].decode('utf-8').split(':')
    else:
        filename = encoded.split(bytearray(':', 'utf-8'))[0].decode('utf-8')
        excess_bits = None
    if dest:
        filename = os.path.join(dest, filename)
    to_create = os.path.split(filename)[0]
    if not os.path.exists(to_create):
        os.makedirs(to_create)
    if not excess_bits:
        open(filename, 'w').close()
        return
    encoded = encoded[filename_end_index + 1:]
    encoded_to_bits = ''
    for elem in encoded:
        to_bits = bin(elem).lstrip('0b')
        encoded_to_bits += '0' * (8 - len(to_bits)) + to_bits
    if excess_bits != '0':
        encoded_to_bits = encoded_to_bits[:-int(excess_bits)]
    try:
        with open(filename, 'w', encoding='utf-8') as f:
            f.write(decode(encoded_to_bits, tree))
    except FileNotFoundError:
        pass


def dir_decompression(path: str, tree: Node = None, dest: str = None) -> str:
    """Decompress a directory

    Args:
        path (str): Path to the directory
        tree (Node, Optional): Huffman tree
        dest (str, Optional): Destination path

    Returns:
        str: Path to the decompressed project
    """
    base_path = '_'.join(path.split('_')[:-1])
    if not tree:
        tree = get_tree_from_file(base_path + '_preorder', base_path + '_inorder')
    with open(path, 'rb') as f:
        content = f.read()
    files_content = content.split(bytearray('\n===\n', 'utf-8'))[:-1]
    if not dest:
        decompressed_path = base_path + '_decompressed'
    else:
        decompressed_path = os.path.join(dest, os.path.split(base_path)[1] + '_decompressed')
    os.mkdir(decompressed_path)
    for file_content in files_content:
        decompress(file_content, tree, dest=decompressed_path)
    return decompressed_path


if __name__ == '__main__':
    # print(dir_compression_to_file('../test/caldera'))
    dir_decompression('../test/caldera_compressed/caldera_compressed', dest='../test')
