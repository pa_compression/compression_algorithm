from enum import Enum


class CompressionEnum(Enum):
    COMPRESSION = '_compressed'
    DECOMPRESSION = '_decompressed'
