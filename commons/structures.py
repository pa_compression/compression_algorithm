# Files for structures needed for Huffman algorithm


class Node:
    """Tree node for Huffman core"""

    def __init__(self, value, weight):
        """
        Args:
            value (char): character the node represents
            weight (int): number of occurrences

        Raises:
            ValueError: Negative weight
        """
        if weight < 0:
            raise ValueError('Weight can\'t be negative')
        self._value = value
        self._weight = weight
        self._left_child = None
        self._right_child = None

    @property
    def value(self):
        """Getter for value"""
        return self._value

    @property
    def weight(self):
        """Getter for weight"""
        return self._weight

    @property
    def right_child(self):
        """Getter for right_child"""
        return self._right_child

    @property
    def left_child(self):
        """Getter for left_child"""
        return self._left_child

    @left_child.setter
    def left_child(self, node):
        """Setter for left_child"""
        self._left_child = node

    @right_child.setter
    def right_child(self, node):
        """Setter for right_child"""
        self._right_child = node

    # TODO UT
    def __str__(self):
        res = '{}:{}\n'.format(self.value, self.weight)
        if self.left_child:
            res += str(self.left_child)
        if self.right_child:
            res += str(self.right_child)
        return res

    def in_order_display(self):
        """In order display of the tree

        Returns:
            str: In order traversal of the tree
        """
        res = ''
        if self.left_child:
            res += self.left_child.in_order_display()
        res += '{}:{}\n'.format(self.value, self.weight)
        if self.right_child:
            res += self.right_child.in_order_display()
        return res


class Heap:
    """Min Heap structure"""

    def __init__(self):
        self._heap = list()

    @property
    def heap(self):
        """Getter for heap"""
        return self._heap

    def __len__(self):
        """Get the number of nodes in the heap"""
        return len(self.heap)

    def __getitem__(self, item):
        return self.heap[item]

    def add(self, node):
        """Add a node to the heap

        Args:
            node (Node): node to insert
        """
        self.heap.append(node)
        index_insert = len(self) - 1
        index_father = (index_insert - 1) // 2
        while index_insert:
            if self[index_father].weight > self[index_insert].weight:
                self.heap[index_father], self.heap[index_insert] = self.heap[index_insert], self.heap[index_father]
                index_insert = index_father
                index_father = (index_insert - 1) // 2
            else:
                break

    def pop(self):
        """Return the min Node

        Raises:
            IndexError: Empty heap
        """
        if len(self) != 0:
            return self.heap.pop(0)
        raise IndexError('Heap is empty')
