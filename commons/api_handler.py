import requests
import json
from conf import conf


class APIWrapper:
    """API calls wrapper"""

    def __init__(self, token: str, env: str):
        """
        Args:
             token (str): Authentication token
             env (str): Running environnement
        """
        self._header = {'Authorization':'Bearer {}'.format(token), 'content-type': 'application/json'}
        self._env = env

    @property
    def header(self):
        return self._header

    @property
    def env(self):
        return self._env

    def post(self, endpoint: str, payload: dict):
        """Post call

        Args:
            endpoint (str): API endpoint,
            payload (str): Body
        """
        response = requests.post('{}/{}'.format(conf['api'][self.env], endpoint),
                                 data=json.dumps(payload),
                                 headers=self.header)
        response.raise_for_status()

    def put(self, endpoint: str):
        """Put call

        Args:
            endpoint (str): API endpoint,
            payload (str): Body
        """
        response = requests.put('{}/{}'.format(conf['api'][self.env], endpoint),
                                headers=self.header)
        response.raise_for_status()
