import unittest

from commons.structures import Node, Heap


class TestNode(unittest.TestCase):
    """Unit Test for Node"""

    test = Node('a', 2)

    def test_init_success(self):
        self.assertIsInstance(self.test, Node)
        self.assertEqual('a', self.test._value)
        self.assertEqual(2, self.test._weight)
        self.assertIsNone(self.test._left_child)
        self.assertIsNone(self.test._right_child)

    def test_init_negative_weight(self):
        self.assertRaises(ValueError, Node, 'a', -1)

    def test_value_getter(self):
        self.assertEqual('a', self.test.value)

    def test_weight_getter(self):
        self.assertEqual(2, self.test.weight)

    def test_left_child_getter(self):
        self.assertIsNone(self.test.left_child)

    def test_right_child_getter(self):
        self.assertIsNone(self.test.right_child)

    def test_left_child_setter(self):
        self.assertIsNone(self.test.left_child)
        left_child = Node('b', 1)
        self.test.left_child = left_child
        self.assertEqual(left_child, self.test.left_child)

    def test_right_child_setter(self):
        self.assertIsNone(self.test.right_child)
        right_child = Node('b', 1)
        self.test.right_child = right_child
        self.assertEqual(right_child, self.test.right_child)


class TestHeap(unittest.TestCase):
    """Unit Test for Heap"""

    heap = Heap()

    def test_init(self):
        self.assertIsInstance(self.heap, Heap)
        self.assertEqual(0, len(self.heap._heap))

    def test_heap_getter(self):
        self.assertEqual([], self.heap.heap)

    def test_len(self):
        self.assertEqual(0, len(self.heap))

    def test_add_to_empty_heap(self):
        self.assertEqual(0, len(self.heap))
        node = Node('a', 3)
        self.heap.add(node)
        self.assertEqual(1, len(self.heap))
        self.assertEqual(node, self.heap[0])
        self.heap.pop()

    def test_add_before_last(self):
        self.assertEqual(0, len(self.heap))
        node1 = Node('b', 3)
        self.heap.add(node1)
        self.assertEqual(1, len(self.heap))
        self.assertEqual(node1, self.heap[0])
        node2 = Node('a', 1)
        self.heap.add(node2)
        self.assertEqual(2, len(self.heap))
        self.assertEqual(node2, self.heap[0])
        self.heap.pop()
        self.heap.pop()

    def test_add_last(self):
        self.assertEqual(0, len(self.heap))
        node1 = Node('a', 1)
        self.heap.add(node1)
        self.assertEqual(1, len(self.heap))
        self.assertEqual(node1, self.heap[0])
        node2 = Node('b', 2)
        self.heap.add(node2)
        self.assertEqual(2, len(self.heap))
        self.assertEqual(node2, self.heap[1])
        self.heap.pop()
        self.heap.pop()

    def test_pop_empty(self):
        self.assertEqual(0, len(self.heap))
        self.assertRaises(IndexError, self.heap.pop)

    def test_pop_not_empty(self):
        self.assertEqual(0, len(self.heap))
        node = Node('a', 3)
        self.heap.add(node)
        self.assertEqual(1, len(self.heap))
        pop_node = self.heap.pop()
        self.assertEqual(0, len(self.heap))
        self.assertEqual(node, pop_node)


if __name__ == '__main__':
    unittest.main()
