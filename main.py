import argparse

from executioner import Executioner


parser = argparse.ArgumentParser(description='Compress and decompress git projects')

parser.add_argument('-c',
                    '--compression',
                    help='Compression task flag',
                    action='store_true')

parser.add_argument('-d',
                    '--decompression',
                    help='Decompression task flag',
                    action='store_true')

parser.add_argument('--s3-bucket',
                    help='AWS S3 bucket name',
                    action='store',
                    type=str,
                    required=True)

parser.add_argument('--aws-access-key',
                    help='AWS access key id',
                    action='store',
                    type=str)

parser.add_argument('--aws-secret-key',
                    help='AWS secret key',
                    action='store',
                    type=str)

parser.add_argument('--git-url',
                    help='URL to the git project',
                    action='store',
                    type=str)

parser.add_argument('--git-username',
                    help='Git username',
                    action='store',
                    type=str)

parser.add_argument('--git-token',
                    help='Git token',
                    action='store',
                    type=str)

parser.add_argument('--aws-folder-location',
                    help='AWS S3 bucket folder to decompress',
                    action='store',
                    type=str)

parser.add_argument('--id',
                    help='Task id',
                    action='store',
                    type=str,
                    required=True)

parser.add_argument('-t',
                    '--token',
                    help='Authentication token',
                    action='store',
                    type=str)

parser.add_argument('-e',
                    '--env',
                    help='Running environnement',
                    action='store',
                    type=str)

args = parser.parse_args()

if not args.compression and not args.decompression:
    print('No task specified')
else:
    o = Executioner(args.s3_bucket,
                    (args.aws_access_key, args.aws_secret_key),
                    git_url=args.git_url,
                    git_username=args.git_username,
                    git_token=args.git_token,
                    api_token=args.token,
                    task_id=args.id)
    if args.compression:
        o.run('compression')
    elif args.decompression:
        if not args.aws_folder_location:
            print('No AWS S3 bucket specified')
        else:
            o.run('decompression', aws_s3_location=args.aws_folder_location)
