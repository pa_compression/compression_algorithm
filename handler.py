import boto3
import logging
import subprocess
from signal import signal, SIGINT, SIGTERM
from datetime import datetime
import json
import os
from oauth import TokenHandler
from conf import conf

sqs = boto3.resource("sqs")
queue = sqs.get_queue_by_name(QueueName=conf['queue'])

class SignalHandler:
    def __init__(self):
        self.received_signal = False
        signal(SIGINT, self._signal_handler)
        signal(SIGTERM, self._signal_handler)

    def _signal_handler(self, signal, frame):
        print(f"handling signal {signal}, exiting gracefully")
        self.received_signal = True

def process_message(body: str, access_token: str):

    body = json.loads(body)

    if 'taskId' not in body:

            raise Exception("No task id found on request")

    if 'compression' in body and body['compression']:

        if 'gitUrl' not in body:

            raise Exception("No git url found on request")

        subprocess.Popen([
            "python3", 
            "/opt/app/main.py", 
            "-c",
            "--token", access_token,
            "--id", body['taskId'],
            "--s3-bucket", conf['bucket'],
            "--git-url", body['gitUrl']])

    if 'decompression' in body and body['decompression']:

        if 'folderLocation' not in body:

            raise Exception("No folder location found on request")

        subprocess.Popen([
            "python3", 
            "/opt/app/main.py", 
            "-d",
            "--token", access_token,
            "--id", body['taskId'],
            "--s3-bucket", conf['bucket'],
            "--aws-folder-location", body['folderLocation']])

        # print("execute decompression task")

if __name__ == "__main__":
    signal_handler = SignalHandler()
    token_handler = TokenHandler(conf['oauth_client'], conf['oauth_secret'],conf['oauth_server'])
    token_handler.fetchToken()

    while not signal_handler.received_signal:
        messages = queue.receive_messages(
            MaxNumberOfMessages=5,
            WaitTimeSeconds=20
        )

        if token_handler.needRefresh():

            logging.info(f"{datetime.now().strftime('%d-%m-%Y %H:%M:%S')} - Refreshing token")
            token_handler.refreshToken()

        for message in messages:
            try:
                logging.info(f"{datetime.now().strftime('%d-%m-%Y %H:%M:%S')} - Message start process - { message.message_id }")
                process_message(message.body, token_handler.getToken()['access_token'])
            except Exception as e:
                logging.warning(f"{datetime.now().strftime('%d-%m-%Y %H:%M:%S')} - exception while processing message: {repr(e)}")
                continue

            message.delete()