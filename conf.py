import json
from os import path


def load_conf() -> dict:
    """Load conf file

    Returns:
        dict: Conf file variables
    """
    if path.exists("/opt/conf/conf.json"):

        with open('/opt/conf/conf.json') as f:
            content = json.load(f)
    else:

        with open('conf.json') as f:
            content = json.load(f)


    return content


conf = load_conf()
