from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
import time;

class TokenHandler:

    def __init__(self, client_id, client_secret, token_url):

        self.client_id = client_id
        self.client_secret = client_secret
        self.token_url = token_url
        self.token = None


    def getToken(self):

        if self.token == None:

            raise Exception("No token set")

        return self.token

    def fetchToken(self):

        try:

            client = BackendApplicationClient(client_id=self.client_id)
            oauth = OAuth2Session(client=client)
            self.token = oauth.fetch_token(
                    token_url=self.token_url, 
                    client_id=self.client_id,
                    client_secret=self.client_secret
                )
        except:
            
            raise Exception("Cannot retrieve your token")


    def refreshToken(self):

        print("Start refresh")

        try:
            client = OAuth2Session(self.client_id, token=self.token)
            self.token = client.refresh_token(self.token_url, **{
                'client_id': self.client_id,
                'client_secret': self.client_secret,
            })

        except Exception as e:

            print(e)
            
            raise Exception("Cannot refresh your token")

    def needRefresh(self):

        if self.token != None and self.token['expires_at'] <= time.time():

            return True

        return False

if __name__ == "__main__":

    handler = TokenHandler(_client_id, _client_secret, _token_url)

    handler.fetchToken()

    try:

        while True:

            if handler.needRefresh():

                handler.refreshToken()

            print(handler.getToken())

            time.sleep(30)

    except Exception as e:

        print(e)