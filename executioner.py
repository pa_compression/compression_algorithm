import os
import shutil
import stat
import errno
import boto3
import uuid
import tarfile
from datetime import datetime
from urllib.parse import urlparse
from urllib.error import HTTPError
from git import Repo

from core.compression import dir_compression_to_file, dir_decompression
from conf import conf
from commons.api_handler import APIWrapper


class Executioner:
    """Main Program"""

    def __init__(self,
                 aws_s3_bucket: str,
                 aws_creds: (str, str),
                 api_token: str,
                 task_id: str,
                 git_url: str = None,
                 git_username: str = None,
                 git_token: str = None,
                 env: str = None):
        """
        Args:
            aws_s3_bucket (str): AWS S3 bucket name,
            aws_creds ((str, str)): AWS credentials,
            api_token (str): API authentication token,
            task_id (str): Task id
            git_url (str, Optional): Url to the git project,
            git_username (str, Optional): Git username,
            git_token (str, Optional): Git token,
            env (str): Running environnement
        """
        self._aws_s3_bucket = aws_s3_bucket
        self._aws_creds = aws_creds
        self._git_url = git_url
        self._git_username = git_username
        self._git_token = git_token
        self._logs = ''
        self._env = env if env else 'production'
        self._api = APIWrapper(api_token, self._env)
        self._task_id = task_id

    @property
    def aws_s3_bucket(self):
        return self._aws_s3_bucket

    @property
    def aws_creds(self):
        return self._aws_creds

    @property
    def git_url(self):
        return self._git_url

    @property
    def git_username(self):
        return self._git_username

    @property
    def git_token(self):
        return self._git_token

    @property
    def logs(self):
        return self._logs

    @property
    def env(self):
        return self._env

    @property
    def api(self):
        return self._api

    @property
    def task_id(self):
        return self._task_id

    @logs.setter
    def logs(self, log):
        self._logs = log

    def run(self, task, aws_s3_location: str = None):
        """Runner for the program. Retrieve the waiting tasks and execute them

        Args:
            task (str): compression/decompression
            aws_s3_location (str, Optional): AWS S3 bucket folder to decompress
        """
        if task == 'compression':
            self.execute_compression()
        elif task == 'decompression':
            self.execute_decompression(aws_s3_location)

    def download_project(self, project_name: str) -> str:
        """Clone the project from git

        Args:
            project_name (str): Folder where the project is downloaded

        Returns:
            str: Project directory
        """
        url_parsed = urlparse(self.git_url)
        if self.git_username and self.git_token:
            url_parsed._replace(netloc='{}:{}@{}'.format(self.git_username,
                                                         self.git_token,
                                                         url_parsed.netloc))
            git_url = url_parsed.geturl()
        else:
            git_url = self.git_url
        os.mkdir(project_name)
        Repo.clone_from(git_url, project_name)
        shutil.rmtree(os.path.join(project_name, '.git'), onerror=error_remove_read_only)
        return project_name

    def upload_to_s3(self,
                     source: [str],
                     project_name: str = None,
                     aws_client=None):
        """Upload file to AWS S3 bucket

        Args:
            source ([str]): Directory to upload
            project_name (str, Optional): Name of the project,
            aws_client (Optional): AWS resource object
        """
        if not project_name:
            project_name = '_'.join(os.path.split(source)[1].split('_'))
            s3_dest = project_name
        else:
            s3_dest = source[source.index(project_name):]
        if not aws_client:
            if self.aws_creds != (None, None):
                session = boto3.Session(aws_access_key_id=self.aws_creds[0],
                                        aws_secret_access_key=self.aws_creds[1])
                aws_client = session.client('s3')
            else:
                aws_client = boto3.client('s3')
        if os.path.isfile(source):
            aws_client.upload_file(source, self.aws_s3_bucket, s3_dest)
        else:
            for file in os.listdir(source):
                self.upload_to_s3('{}/{}'.format(source, file),
                                  project_name,
                                  aws_client)

    def execute_compression(self):
        """Execute compression task"""
        folder_name = str(uuid.uuid4())
        project_name = '{}'.format(folder_name)
        compressed_path = '/tmp/{}_compressed'.format(folder_name)
        tar_path = '{}.tar.gz'.format(compressed_path)
        try:
            self.api.put(conf['api']['endpoints']['status_update'].format(self.task_id, 1))
            self.logs = '{} > Downloading {}\n'.format(datetime.now(), self.git_url)
            path = self.download_project(project_name)
            self.logs += '{} > Starting compression\n'.format(datetime.now())
            compressed_path = dir_compression_to_file(path)
            self.logs += '{} > Archiving folder\n'.format(datetime.now())
            with tarfile.open(tar_path, 'w:gz') as tar:
                tar.add(compressed_path, arcname=os.path.split(compressed_path)[1])
            self.logs += '{}\n'.format(tar_path)
            self.logs += '{} > Uploading {} to S3\n'.format(datetime.now(), os.path.split(compressed_path)[1])
            self.upload_to_s3(tar_path)
            self.api.post(conf['api']['endpoints']['file'].format(self.task_id), dict(folderUuid=folder_name))
        except Exception as e:
            self.logs += '{} > {}\n'.format(datetime.now(), e)
            self.api.put(conf['api']['endpoints']['status_update'].format(self.task_id, 3))
        finally:
            with open(conf['logs_path'], 'a') as f:
                f.write(self.logs)
            if os.path.exists(project_name):
                shutil.rmtree(project_name)
            if os.path.exists(compressed_path):
                shutil.rmtree(compressed_path)
            if os.path.exists(tar_path):
                os.remove(tar_path)


    def download_from_s3(self, folder: str) -> str:
        """Download files from AWS S3

        Args:
            folder (str): AWS S3 folder to download

        Returns:
            str: Path to the downloaded folder
        """
        # os.mkdir(folder)
        if self.aws_creds != (None, None):
            s3 = boto3.resource('s3',
                                aws_access_key_id=self.aws_creds[0],
                                aws_secret_access_key=self.aws_creds[1])
        else:
            s3 = boto3.resource('s3')
        aws_bucket = s3.Bucket(self.aws_s3_bucket)
        aws_bucket.download_file(folder, '/tmp/{}'.format(folder))
        #for object in aws_bucket.objects.filter(Prefix=folder):
        #    aws_bucket.download_file(object.key, object.key)
        return folder

    def execute_decompression(self, aws_location: str):
        """Execute decompression task

        Args:
            aws_location (str): AWS S3 bucket folder to download
        """
        decompressed_path_tar = '/tmp/{}'.format('_'.join(aws_location.split('_')[:-1]))
        decompressed_path = os.path.splitext(os.path.splitext(decompressed_path_tar)[0])[0] + '_compressed'
        tar_path = '{}_decompressed.tar.gz'.format(decompressed_path)
        try:
            self.api.put(conf['api']['endpoints']['status_update'].format(self.task_id, 1))
            self.logs = '{} > Downloading {} from S3\n'.format(datetime.now(), aws_location)
            path = os.path.join(self.download_from_s3('{}.tar.gz'.format(aws_location)), aws_location)
            self.logs += '{} > Unarchiving\n'.format(datetime.now())
            print(decompressed_path)
            with tarfile.open('/tmp/{}.tar.gz'.format(aws_location), 'r:gz') as tar:
                tar.extractall(decompressed_path)
            self.logs += '{} > Starting decompression\n'.format(datetime.now())
            decompressed_path = dir_decompression(os.path.join(
                decompressed_path,
                '{}'.format(os.path.split(decompressed_path)[1]),
                '{}'.format(os.path.split(decompressed_path)[1])
            ))
            self.logs += '{} > Archiving file_n'.format(datetime.now())
            with tarfile.open(tar_path, 'w:gz') as tar:
                tar.add(decompressed_path, arcname=os.path.split(decompressed_path)[1])
            self.logs += '{} > Uploading {} to S3\n'.format(datetime.now(), decompressed_path)
            self.upload_to_s3(tar_path)
            self.api.put(conf['api']['endpoints']['finish_uncompress'].format(self.task_id))
        except Exception as e:
            self.api.put(conf['api']['endpoints']['status_update'].format(self.task_id, 3))
            self.logs += '{} > {}\n'.format(datetime.now(), e)
        finally:
            with open(conf['logs_path'], 'a') as f:
                f.write(self.logs)
            if os.path.exists(aws_location):
                shutil.rmtree(aws_location)
            if os.path.exists(decompressed_path):
                shutil.rmtree(decompressed_path)
            if os.path.exists(tar_path):
                shutil.rmtree(tar_path)
            if os.path.exists(decompressed_path_tar):
                shutil.rmtree(decompressed_path_tar)


def error_remove_read_only(func, path, exc):
    """Edit file permissions

    Args:
        func (Callable): Function used
        path (str): Path to the file
        exc : Execution info
    """
    excvalue = exc[1]
    if excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
        func(path)
    else:
        raise PermissionError(excvalue.errno)


if __name__ == '__main__':
    # o.download_project('https://gitlab.com/pa_compression/compression_algorithm.git', username='awang96', token='L6DP5r2JhG985cPUxnQq')
    # o.execute_decompression('pa-compression',
    #                         ('aze', 'aaze'),
    #                         'compression_algorithm_compressed')
    o = Executioner('pa-compression',
                    ('aze', 'aze'),
                    'aze',
                    'aze',
                    git_url='https://gitlab.com/pa_compression/compression_algorithm.git')
    # o.run('compression')
    # o.execute_decompression('compression_algorithm_compressed')
    # o.download_project()
    # o.execute_compression()
    o.execute_decompression('f1299f89-dbf6-43f9-adcc-efeee94be9ea_compressed')
