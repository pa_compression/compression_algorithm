import argparse
import json

parser = argparse.ArgumentParser(description='Configuration file editor')

parser.add_argument('--host',
                    help='Production host',
                    action='store',
                    type=str)

parser.add_argument('-q',
                    '--queue',
                    help='AWS SNS Queue name',
                    action='store',
                    type=str)

parser.add_argument('-b',
                    '--bucket',
                    help='AWS S3 bucket name',
                    action='store',
                    type=str)

parser.add_argument('--oauth-server',
                    help='Oauth server URL',
                    action='store',
                    type=str)

parser.add_argument('--oauth-client',
                    help='Oauth client id',
                    action='store',
                    type=str)

parser.add_argument('--oauth-secret',
                    help='Oauth secret',
                    action='store',
                    type=str)

parser.add_argument('-f',
                    '--file-path',
                    help='Path to the configuration file',
                    action='store',
                    type=str,
                    required=True)


def append_to_dict(conf: dict, key: str, value: str) -> dict:
    """Add key:value to conf

    conf (dict): Dictionnary you want to add key:value to,
    key (str): Key you want to add
    value (str): Value you want to add
    """
    if not value:
        return conf
    conf[key] = value
    return conf


args = parser.parse_args()

with open(args.file_path) as f:
    conf = json.load(f)

conf['api'] = append_to_dict(conf['api'], 'production', args.host)
conf = append_to_dict(conf, 'queue', args.queue)
conf = append_to_dict(conf, 'bucket', args.bucket)
conf = append_to_dict(conf, 'oauth_server', args.oauth_server)
conf = append_to_dict(conf, 'oauth_client', args.oauth_client)
conf = append_to_dict(conf, 'oauth_secret', args.oauth_secret)

with open(args.file_path, 'w') as f:
    json.dump(conf, f, indent=4)
